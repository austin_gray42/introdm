import kmeans

points = []

for line in open("../data/uber-raw-data-apr14.csv"):
    point = []
    for value in line.split(","):
        point.append(float(value))
    points.append(tuple(point))
        

clusters = kmeans.KMeans(points, 3)


#exports results to csv file, marking every point with cluster number
f= open('../data/uber_kmeans.csv', 'w')
marker = 0
for cluster in clusters:
    for point in cluster:
        for column in point:
            f.write(str(column)+',')
        f.write(str(marker)+'\n')
    marker+=1
f.close()
