import kmeans

points = []

for line in open("../data/wine.csv"):
    point = []
    for value in line.split(","):
        point.append(float(value))
    points.append(tuple(point))
        

clusters = kmeans.KMeans(points, 3)

#print(clusters[0])


#exporting result to csv file, marking by cluster
f= open('../data/wine_kmeans.csv', 'w')
marker=0
for cluster in clusters:
    for point in cluster:
        for column in point:
            f.write(str(column) +',')
        f.write(str(marker)+'\n')
    marker+=1
f.close()
        
        
            
