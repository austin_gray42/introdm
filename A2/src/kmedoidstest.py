import kmedoids

points = []

for line in open("../data/wine.csv"):
    point = []
    for value in line.split(","):
        point.append(float(value))
    points.append(tuple(point))
        

clusters = kmedoids.KMedoids(points, 3)

print(clusters[0])
