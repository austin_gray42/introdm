#A template for the implementation of K-Means.
import math #sqrt


# Accepts two data points a and b.
# Returns the euclidian distance 
# between a and b.
def dist(a,b):
    length = len(a)
    my_sum=0
    for num in range(length):
        my_sum += (b[num]-a[num])**2

    return math.sqrt(my_sum)



# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
    clusters = {}
    for c in centers:
        clusters[c] = []
        
    for point in D:
        nearest = centers[0]
        for c in centers:
            if dist(c, point) < dist(nearest, point):
                nearest = c
        clusters[nearest].append(point)
    
    return clusters

# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
    pointlength = len(cluster[0])
    clusterlength = len(cluster)
    mean = []
    for num in range(pointlength):
        my_sum=0
        for point in cluster:
            my_sum += point[num]
        mean.append(my_sum/clusterlength)
    return mean

#converts list of lists into lists of tuples, needed because lists cannot be dictionary keys
def tupleConvert(old_list):
    new_list = []
    for l in old_list:
        new_list.append(tuple(l))
    return new_list
        

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
    means = tupleConvert(D[0:k])

    while True:     #"Tom" on StackOverflow told us that do-while loops could be implemented in Python with a while loop ending with an if condition to break. Thanks Tom.
        #print("working...")
        oldmeans = set(tupleConvert(means))
        clusters = assignClusters(D, means)
        means=[]
        for cluster in clusters.values():
            means.append(findClusterMean(cluster))
            
        means = tupleConvert(means)
        
        newmeans = set(means)

        if newmeans == oldmeans:
            return list(clusters.values())
    





