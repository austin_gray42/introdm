import hclust

points = []

for line in open("../data/uber-raw-data-apr14.csv"):
    point = []
    for value in line.split(","):
        point.append(float(value))
    points.append(tuple(point))
        

clusters = hclust.HClust(points)

#exports dictionary of tree structure to text file
f=open('../data/uber_hclust.txt', 'w')
for pair in clusters:
    f.write(str(pair)+'\n')
f.close()

#runs forever
