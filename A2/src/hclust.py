#A template for the implementation of hclust
import math #sqrt


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    length = len(a)
    my_sum=0
    for num in range(length):
        my_sum += (b[num]-a[num])**2

    return math.sqrt(my_sum)

# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b):
    length = len(a)
    avg_point = []
    for num in range(length):
        avg = (a[num] + b[num])/2
        avg_point.append(avg)
    return tuple(avg_point)

# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
    smallest_dist=10000000000000
    closest_points=None
    for point in D:
        D2 = D.copy()
        D2.remove(point)
        for point2 in D2:
            dist = Distance(point, point2)
            if dist < smallest_dist:
                smallest_dist = dist
                closest_points = [point, point2]
    return closest_points


# Accepts a list of data points.
# Produces a tree structure corresponding to a 
# Agglomerative Hierarchal clustering of D.
def HClust(D):
    centers = D.copy()
    splits= {}

    while True:
        print(len(centers))
        location=findClosestPair(centers)
        for point in location:
            centers.remove(point)
        merge_point = merge(location[0],location[1])
        centers.append(merge_point)
        splits[merge_point]=location

        if len(centers) == 1:
            return splits
            
    



