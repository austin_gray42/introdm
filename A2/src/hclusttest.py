import hclust

points = []

for line in open("../data/wine.csv"):
    point = []
    for value in line.split(","):
        point.append(float(value))
    points.append(tuple(point))
        

cluster_tree_structure = hclust.HClust(points)

#print(cluster_tree_structure)
