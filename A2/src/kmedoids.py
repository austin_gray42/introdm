#A template for the implementation of K-Medoids.
import math #sqrt


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    length = len(a)
    my_sum=0
    for num in range(length):
        my_sum += (b[num]-a[num])**2

    return math.sqrt(my_sum)


# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
    clusters = {}
    for c in centers:
        clusters[c] = []
        
    for point in D:
        nearest = centers[0]
        for c in centers:
            if Distance(c, point) < Distance(nearest, point):
                nearest = c
        clusters[nearest].append(point)
    
    return clusters

# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(cluster):
    medoid=None
    small_avg=10000000000000000000000000000
    for point in cluster:
        my_sum=0
        for point2 in cluster:
            my_sum+=Distance(point,point2)
        avg_dist=my_sum/(len(cluster)-1)
        if avg_dist<small_avg:
            medoid=point
            small_avg=avg_dist
    return medoid
        
        
        

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
    medoids=D[0:k]
    while True:
        print(medoids)
        oldmedoids=set(medoids)
        clusters=assignClusters(D,medoids)
        medoids=[]
        for cluster in clusters.values():
            medoids.append(findClusterMedoid(cluster))

        new_medoids = set(medoids)
        if new_medoids == oldmedoids:
            return list(clusters.values())
        
            
    
				



