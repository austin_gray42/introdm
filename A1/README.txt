Please provide instructions here for how to build and run your project.

The preferred build system is make.

If you do not know make, any other single command that will build and run your project is acceptable.



INSTRUCTIONS:

If you just go into the src directory and run movie_mining.py and dota_mining.py in python, they will use the apriori allgorithm to find rules and then print them.

movie_mining.py creates rules based on the movie data

dota_mining creates rules based on the Dota 2 match data I collected myself, specifically looking at team compositions in matches

There are no prerequisites  for these programs outside of the standard Python installation


In the data directory I have placed my datasets as well as the Python script I used to interact with the Dota 2 API. That program will not run since the dota2api package
is a requirement, but you should not run it anyways as it might mess up the project. The data collected from this program was stored in DotaData.csv, so the program itself
is no longer necessary.