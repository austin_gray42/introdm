import apriori

#creates translation dictionary of movie ids and movie names
movie_names = open('../data/movielisting.txt', 'r')
movie_dict = {}
for line in movie_names:
    (movie_id, movie_name) = line.split(" ", 1)
    movie_dict[movie_id] = movie_name.rstrip()
movie_names.close()


movie_data = open('../data/Movies.dat', 'r')
movie_sets = {}
for line in movie_data:
    (user_id, movie_id) = line.split()
    if user_id in movie_sets:
        movie_sets[user_id].add(movie_dict[movie_id])
    else:
        movie_sets[user_id] = set([movie_dict[movie_id]])
movie_data.close()

print('now looking for frequent sets...')

frequent_movies = apriori.findFrequentItemsets(movie_sets.values(), 300)

#print(frequent_movies)

print('now determining rules')

rules = apriori.findRules(frequent_movies, movie_sets.values(), 0.8)

apriori.visualizeRules(rules)


        
        
