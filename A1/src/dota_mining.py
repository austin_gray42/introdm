import apriori
import time

dota_data = open('../data/DotaData.csv', 'r')
hero_sets = []
for line in dota_data:
    heroes = line.rstrip().split(',')
    heroes.remove('')
    hero_sets.append(set(heroes))
dota_data.close()

print('now looking for frequent sets...')

frequent_heroes = apriori.findFrequentItemsets(hero_sets, 200)

print('now determining rules...')

rules = apriori.findRules(frequent_heroes, hero_sets, 0.2)

apriori.visualizeRules(rules)

    

