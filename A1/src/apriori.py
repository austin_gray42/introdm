#Template for Assignment 1.
#Author:
#Date:
#Note: This implementation is not very efficient. 
# Hint: @lru_cache(maxsize=None) is likely to be a 
#   favourable decoration for some functions.


#Computes the support of the given itemset in the given database.
#itemset: A set of items
#database: A list of sets of items
#return: The number of sets in the database which itemset is a subset of. 
def support(itemset, database):
    count = 0
    for dataset in database:
        if itemset.issubset(dataset):
            count += 1
    return count


#Computes the confidence of a given rule.
#The rule takes the form precedent --> antecedent
#precedent: A set of items
#antecedent: A set of items that is a superset of precedent
#database: a list of sets of items.
#return: The confidence in precedent --> antecedent.
def confidence(precedent, antecedent, database):
    return (support(antecedent, database) / support(precedent, database))

#Finds all itemsets in database that have at least minSupport.
#database: A list of sets of items.
#minSupport: an integer > 1
#return: A list of sets of items, such that 
#   s in return --> support(s,database) >= minSupport.
def findFrequentItemsets(database, minSupport):
    fsets=[]
    checkedsets=[]
    for itemset in database:
        for item in itemset:
            singleset = set([item])
            if singleset not in checkedsets:
                if support(singleset, database)>= minSupport:
                    fsets.append(singleset)
                    #print("Added 1")
                checkedsets.append(singleset)
            #print('...')
    #print(fsets)
    for fset in fsets:
        for fset2 in fsets:
            testset = fset.union(fset2)
            if testset not in checkedsets:
                if support(testset, database) >= minSupport:
                    fsets.append(testset)
                    #print('Added 2')
                checkedsets.append(testset)
    print("Frequent sets found: " + str(len(fsets)))
    return fsets
                

#Given a set of frequently occuring Itemsets, returns
# a list of pairs of the form (precedent, antecedent)
# such that for every returned pair, the rule 
# precedent --> antecedent has confidence >= minConfidence
# in the database.
#frequentItemsets: a set or list of sets of items.
#database: A list of sets of items.
#minConfidence: A real value between 0.0 and 1.0. 
#return: A set or list of pairs of sets of items.
def findRules(frequentItemsets, database, minConfidence):
    rules = []

    for fset in frequentItemsets:
        for fset2 in frequentItemsets:
            if fset!=fset2 and fset.issubset(fset2):
                if confidence(fset, fset2, database) >= minConfidence:
                    rules.append((fset, fset2, confidence(fset, fset2, database)))
    
    return rules


#Produces a visualization of frequent itemsets.
def visualizeItemsets(frequentItemsets):
    for itemset in frequentItemsets:
        print(itemset)

#Produces a visualization of rules.
def visualizeRules(rules):
    for rule in rules:
        print(str(rule[0]) + " ====> " + str(rule[1]) + " Confidence: " +str(rule[2]))
    if len(rules) == 0:
        print('no rules')


    

        
