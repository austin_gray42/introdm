import dota2api
import time

key= 'E0D62C764B3054F24A048900E4DFD751'
api = dota2api.Initialise(key)


#first we create a list of hero names with their index positions matching their hero ID numbers
hero_dict_list = api.get_heroes()['heroes']

hero_list = ['Nobody']

for d in hero_dict_list:
    
    hero_list.append(d['localized_name'])

#this hero possibly shows up in data but is not available from api call
hero_list.append('Monkey King')

#print(len(hero_list))


#takes a single match dictionary and returns the set of players' hero choices
def get_hero_set(match_d):
    hero_set = set()
    
    for player in match_d['players']:
        #print(player['hero_id'])
        h = hero_list[player['hero_id']]
        if player['player_slot'] < 5 :
            hero_set.add("Radiant " + h)
        else:
            hero_set.add("Dire " + h)
    return hero_set

#for fun we will make a version of the hero sets which include which team won
def get_hero_set_with_winner(match_d):
    hs = get_hero_set(match_d)
    if match_d['radiant_win']:
        hs.add("Rwin")
    else:
        hs.add("Dwin")

    return hs

#start_at_match_seq_num=2297291941

setsize = 10000
wait_time = 5
pool = api.get_match_history_by_seq_num(start_at_match_seq_num=2297291941 - setsize)
time.sleep(wait_time)
print((len(pool['matches'])))
matches = []
matches.extend(pool['matches'])
#print(matches[0]['match_seq_num'], matches[1]['match_seq_num'])
#print(matches[0]['start_time'], matches[1]['start_time'])


while len(matches) < setsize:
    last_id = ((pool['matches'][99]['match_seq_num']) + 1)
        
    pool = api.get_match_history_by_seq_num(start_at_match_seq_num=last_id)
    time.sleep(wait_time)
    
    matches.extend(pool['matches'])
    print(len(matches))

hero_sets = []
hero_sets_with_winners= []

check_set = set()
for match in matches:
    check_set.add(match['match_id'])
    
    if match['human_players'] == 10:
        
        hero_sets.append(get_hero_set(match))
        hero_sets_with_winners.append(get_hero_set_with_winner(match))

#the checkset just makes sure there are no matches that are used in the data more than once
if len(check_set) < len(matches):
    print('Warning! There are repeats!')
    print(str(len(check_set)) + ' unique matches out of ' + str(len(matches)))

print("hero_sets length: " + str(len(hero_sets)))

f = open('DotaData.csv', 'w')
for hero_set in hero_sets:
    for hero in hero_set:
        f.write(hero + ',')
    f.write('\n')
f.close()

f2 = open('DotaDataWin.csv', 'w')
for item_set in hero_sets_with_winners:
    for item in item_set:
        f2.write(item + ',')
    f2.write('\n')
f2.close()
    





